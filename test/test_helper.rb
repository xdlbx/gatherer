ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  #
  # Note: You'll currently still have to declare fixtures explicitly in integration tests
  # -- they do not yet inherit this setting
  fixtures :all

  # Add more helper methods to be used by all tests here...
  ENV["RAILS_ENV"] ||= "test"
  require File.expand_path('../../config/environment', __FILE__)
  require 'rails/test_help'
  require 'minitest/rails/capybara'
  require 'mocha/mini_test'

  require "minitest/reporters"
  Minitest::Reporters.use!
end
